package com.example.mobile

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var lista= arrayListOf<String>("btn_11","btn_12","btn_13","btn_21","btn_22","btn_23","btn_31"
        ,"btn_32","btn_33")
    var listaJugador= arrayListOf<String>()
    var listaBot= arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_11.setOnClickListener{
            btn_11.setBackgroundColor(Color.RED)
            btn_11.text="+";
            listaJugador.add("btn_11")
            removeFromList("btn_11")
            randomButton(it)
            btn_11.isEnabled=false
            verificarJugadas(it)
        }
        btn_12.setOnClickListener{
            btn_12.setBackgroundColor(Color.RED)
            btn_12.text="+";
            listaJugador.add("btn_12")
            removeFromList("btn_12")
            randomButton(it)
            btn_12.isEnabled=false
            verificarJugadas(it)
        }
        btn_13.setOnClickListener{
            btn_13.setBackgroundColor(Color.RED)
            btn_13.text="+";
            listaJugador.add("btn_13")
            removeFromList("btn_13")
            randomButton(it)
            btn_13.isEnabled=false
            verificarJugadas(it)
        }
        btn_21.setOnClickListener{
            btn_21.setBackgroundColor(Color.RED)
            btn_21.text="+";
            listaJugador.add("btn_21")
            removeFromList("btn_21")
            randomButton(it)
            btn_21.isEnabled=false
            verificarJugadas(it)
        }
        btn_22.setOnClickListener{
            btn_22.setBackgroundColor(Color.RED)
            btn_22.text="+";
            listaJugador.add("btn_22")
            removeFromList("btn_22")
            randomButton(it)
            btn_22.isEnabled=false
            verificarJugadas(it)
        }
        btn_23.setOnClickListener{
            btn_23.setBackgroundColor(Color.RED)
            btn_23.text="+";
            listaJugador.add("btn_23")
            removeFromList("btn_23")
            randomButton(it)
            btn_23.isEnabled=false
            verificarJugadas(it)
        }
        btn_31.setOnClickListener{
            btn_31.setBackgroundColor(Color.RED)
            btn_31.text="+";
            listaJugador.add("btn_31")
            removeFromList("btn_31")
            randomButton(it)
            btn_31.isEnabled=false
            verificarJugadas(it)
        }
        btn_33.setOnClickListener{
            btn_33.setBackgroundColor(Color.RED)
            btn_33.text="+";
            listaJugador.add("btn_33")
            removeFromList("btn_33")
            randomButton(it)
            btn_33.isEnabled=false
            verificarJugadas(it)
        }
        btn_32.setOnClickListener{
            btn_32.setBackgroundColor(Color.RED)
            btn_32.text="+";
            listaJugador.add("btn_32")
            removeFromList("btn_32")
            randomButton(it)
            btn_32.isEnabled=false
            verificarJugadas(it)
        }
    }
    fun removeFromList(name:String){
        lista.remove(name);
    }
    fun randomButton(it: View){
        var listlength:Int = lista.size
        if(listlength > 0 )
        {
            var numeroRandom = (0..listlength).random()
            if(numeroRandom==listlength && numeroRandom>0)
            {
                numeroRandom=numeroRandom-1;
            }
            val buttonID = resources.getIdentifier(lista[numeroRandom], "id", packageName)
            var button:Button = findViewById(buttonID)
            button.setBackgroundColor(Color.GREEN)
            button.text="*"
            button.isEnabled=false
            listaBot.add(lista[numeroRandom])
            removeFromList(lista[numeroRandom])
            verificarJugadas(it)
        }
    }
    fun verificarJugadas(it: View){
        val builder = AlertDialog.Builder(it.context)
        if( listaBot.containsAll(listOf("btn_11","btn_12","btn_13")) ||
                    listaBot.containsAll(listOf("btn_21","btn_22","btn_23")) ||
                            listaBot.containsAll(listOf("btn_31","btn_32","btn_33")) ||
                                    listaBot.containsAll(listOf("btn_11","btn_21","btn_31"))||
                                            listaBot.containsAll(listOf("btn_12","btn_22","btn_32") )||
                                                    listaBot.containsAll(listOf("btn_13","btn_23","btn_33")) ||
                                                            listaBot.containsAll(listOf("btn_11","btn_22","btn_33")) ||
                                                                    listaBot.containsAll(listOf("btn_11","btn_22","btn_33"))) {
            builder.setTitle("Ganador MacBot")
            builder.setMessage("Gracias por participar … ")
            builder.setPositiveButton("Reiniciar"){dialog, which ->
                finish();
                startActivity(getIntent())
                Toast.makeText(applicationContext, "Nuevo juego", Toast.LENGTH_LONG).show()
            }
            builder.show()
        }
        if( listaJugador.containsAll(listOf("btn_11","btn_12","btn_13")) ||
                listaJugador.containsAll(listOf("btn_21","btn_22","btn_23")) ||
                     listaJugador.containsAll(listOf("btn_31","btn_32","btn_33")) ||
                            listaJugador.containsAll(listOf("btn_11","btn_21","btn_31"))||
                                    listaJugador.containsAll(listOf("btn_12","btn_22","btn_32"))||
                                            listaJugador.containsAll(listOf("btn_13","btn_23","btn_33")) ||
                                                    listaJugador.containsAll(listOf("btn_11","btn_22","btn_33")) ||
                                                            listaJugador.containsAll(listOf("btn_11","btn_22","btn_33"))) {
            builder.setTitle("Ganador Pandicornios")
            builder.setMessage("Gracias por participar … ")
            builder.setPositiveButton("Reiniciar"){dialog, which ->
                finish();
                startActivity(getIntent())
                Toast.makeText(applicationContext, "Nuevo juego", Toast.LENGTH_LONG).show()
            }
            builder.show()
        }

    }
}
